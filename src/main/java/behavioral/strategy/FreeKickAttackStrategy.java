package behavioral.strategy;

import behavioral.strategy.LoggerManager;
import java.util.logging.Logger;

public class FreeKickAttackStrategy implements GameStrategy {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    @Override
    public void play(int numberOfPlayers) {
        logger.info(String.format("Tiro Libre en ataque con %d jugadores", numberOfPlayers));
    }
}
