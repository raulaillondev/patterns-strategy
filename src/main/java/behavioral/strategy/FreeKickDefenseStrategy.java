package behavioral.strategy;

import behavioral.strategy.LoggerManager;
import java.util.logging.Logger;

public class FreeKickDefenseStrategy implements GameStrategy {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    @Override
    public void play(int numberOfPlayers) {
        String msg =  (numberOfPlayers == 0) ?
            "Tiro Libre en defensa lejos de porteria" :
            String.format("Tiro Libre en defensa con %d jugadores", numberOfPlayers);
        logger.info(msg);
    }
}
