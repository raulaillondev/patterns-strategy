package behavioral.strategy;

@FunctionalInterface
public interface GameStrategy {

  void play(int numberOfPlayers);
}
