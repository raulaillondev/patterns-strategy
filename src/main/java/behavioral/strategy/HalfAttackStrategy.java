package behavioral.strategy;

import behavioral.strategy.LoggerManager;
import java.util.logging.Logger;

public class HalfAttackStrategy implements GameStrategy {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    @Override
    public void play(int numberOfPlayers) {
        logger.info(String.format("Medio ataque con %d jugadores", numberOfPlayers));
    }
}
