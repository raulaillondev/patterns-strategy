package behavioral.strategy;

import behavioral.strategy.LoggerManager;
import java.util.logging.Logger;

public class HitTheBallDefenseStrategy implements GameStrategy {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    @Override
    public void play(int numberOfPlayers) {
        logger.info(String.format("Despejar el balon ante un ataque con %d jugadores", numberOfPlayers));
    }
}
