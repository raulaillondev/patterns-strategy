package behavioral.strategy;

import java.util.logging.Logger;
import java.util.logging.Level;

public class LoggerManager {

    private LoggerManager() {
        throw new IllegalAccessError("Utility class");
    }

    public static void init(){
        System.setProperty(
          "java.util.logging.SimpleFormatter.format",
          "[%2$-53s] [%4$-5s:] %5$s%n"
        );
        Logger l = Logger.getLogger("");
        l.setLevel(Level.CONFIG);
    }
 }
