package behavioral.strategy;

import behavioral.strategy.LoggerManager;
import java.util.logging.Logger;

public class StrategyApp {

    private StrategyApp() {
        throw new IllegalAccessError("Main class");
    }

    public static void main(String[] args) {

        LoggerManager.init();
        Team team = new Team();
        team.play();

        // Tenemos el balon
        team.setGameStrategy(new AttackStrategy());
        team.play(5);

        // Sin balon
        team.setGameStrategy(new DefenseStrategy());
        team.play(8);

        // Medio Ataque
        team.setGameStrategy(new HalfAttackStrategy());
        team.play(1);

        team.setGameStrategy(
            new GameStrategy() {
            private final Logger logger = Logger.getLogger(LoggerManager.class.getName());

            @Override
            public void play(int numberOfPlayers) {
                logger.info("Simula faltas");
            }
            });
        team.play();

        // Tiro libre en ataque cerca del arco rival
        team.setGameStrategy(new FreeKickAttackStrategy());
        team.play(6);

        // Tiro libre en defensa lejos de nuestro arco
        team.setGameStrategy(new FreeKickDefenseStrategy());
        team.play();

        // Despejar el balon en defensa cerca de nuestro arco
        team.setGameStrategy(new HitTheBallDefenseStrategy());
        team.play(2);


        // El match esta por terminar, mantener el balon
        team.setGameStrategy(new KeepPossesionDefenseStrategy());
        team.play(10);



    }
}
