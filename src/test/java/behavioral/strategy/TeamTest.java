package behavioral.strategy;

import behavioral.strategy.StrategyApp;
import behavioral.strategy.AttackStrategy;
import behavioral.strategy.DefenseStrategy;
import behavioral.strategy.HalfAttackStrategy;
import behavioral.strategy.FreeKickAttackStrategy;
import behavioral.strategy.FreeKickDefenseStrategy;
import behavioral.strategy.LoggerManager;

import static org.junit.Assert.assertTrue;

import java.util.logging.Logger;
import java.util.logging.Handler;
import java.util.logging.StreamHandler;
import java.util.ArrayList;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;

import org.junit.Test;
import org.junit.Before;

public class TeamTest {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());
    private OutputStream logOut;
    private StreamHandler testLogHandler;
    private Team team;

    @Before
    public void setUp() throws Exception {
        LoggerManager.init();
        setUpLogHandler(logger);
        team = new Team();
    }

    @Test
    public void testGameStarted() {
        String expected = "Esperar al rival";
        team.play();
        String captured = getLogOutput();

        assertTrue(captured.contains(expected));
    }

    @Test
    public void testPossesionAttack() {
        int players = 5;
        String expected = String.format("Atacar con %d jugadores", players);
        // Tenemos el balon
        team.setGameStrategy(new AttackStrategy());
        team.play(players);
        String captured = getLogOutput();

        assertTrue(captured.contains(expected));
    }

    @Test
    public void testNoPossesionDefense() {
        int players = 7;
        String expected = String.format("Defender con %d jugadores", players);
        // Tenemos el balon
        team.setGameStrategy(new DefenseStrategy());
        team.play(players);
        String captured = getLogOutput();

        assertTrue(captured.contains(expected));
    }

    @Test
    public void testPossesionHalfAttack() {
        int players = 2;
        String expected = String.format("Medio ataque con %d jugadores", players);
        // Medio Ataque
        team.setGameStrategy(new HalfAttackStrategy());
        team.play(players);
        String captured = getLogOutput();

        assertTrue(captured.contains(expected));
    }

    @Test
    public void testNewStrategies() {
        int players = 2;
        String[] expected = new String[5];
        String captured = "";

        expected[0] = String.format("Tiro Libre en ataque con %d jugadores", players);
        // Tiro libre en ataque cerca del arco rival
        team.setGameStrategy(new FreeKickAttackStrategy());
        team.play(players);

        expected[1] = "Tiro Libre en defensa lejos de porteria";
        // Tiro libre en defensa lejos de nuestro arco
        team.setGameStrategy(new FreeKickDefenseStrategy());
        team.play();

        players = 8;
        expected[2] = String.format("Tiro Libre en defensa con %d jugadores", players);
        // Tiro libre en defensa lejos de nuestro arco
        team.setGameStrategy(new FreeKickDefenseStrategy());
        team.play(players);

        expected[3] = String.format("Despejar el balon ante un ataque con %d jugadores", players);
        // Despejar el balon en defensa cerca de nuestro arco
        team.setGameStrategy(new HitTheBallDefenseStrategy());
        team.play(players);

        players = 10;
        expected[4] = String.format("Mantener posesion del balon con %d jugadores", players);
        // El match esta por terminar, mantener el balon
        team.setGameStrategy(new KeepPossesionDefenseStrategy());
        team.play(players);

        captured = getLogOutput();

        for (String aExpectedString : expected) {
            assertTrue(captured.contains(aExpectedString));
        }
    }

    @Test
    public void testStrategyApp() {
        String captured = "";
        String[] expected = new String[] {
            "Esperar al rival",
            "Atacar con 5 jugadores",
            "Defender con 8 jugadores",
            "Medio ataque con 1 jugadores",
            "Simula faltas",
            "Tiro Libre en ataque con 6 jugadores",
            "Tiro Libre en defensa lejos de porteria",
            "Despejar el balon ante un ataque con 2 jugadores",
            "Mantener posesion del balon con 10 jugadores"
        };
        String[] args = null;
        StrategyApp.main(args);
        captured = getLogOutput();

        for (String aExpectedString : expected) {
            assertTrue(captured.contains(aExpectedString));
        }
    }

    protected String getLogOutput(){
        testLogHandler.flush();
        String captured = logOut.toString();
        return captured;
    }

    /**
    Add stream handler to logger.
    Will take more effort then this, e.g., may not have parent handler.
    */
    protected void setUpLogHandler(Logger logger) {
        logOut = new ByteArrayOutputStream();
        Handler[] handlers = logger.getParent().getHandlers();
        testLogHandler = new StreamHandler(logOut, handlers[0].getFormatter());
        logger.addHandler(testLogHandler);
    }
}
